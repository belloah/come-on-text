# Release Notes

## ver 2.0

November 21, 2020

- Changed platform from Github to Gitlab
- Changed theme
- Changed project architecture

## ver 1.0

June 30, 2020

- Added *Add space at the end of line* option for line removal feature in order to facilitate English article input

## ver 0.0

May 25, 2020

- Introduced Come On Text
