import Card from 'react-bootstrap/Card';
import Collapse from 'react-bootstrap/Collapse';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import '@fortawesome/fontawesome-free/css/all.css';
import { useState } from 'react';
import './App.css'
const App = () => {
  const [config, setConfig] = useState(true)
  const [input, setInput] = useState("")
  const [output, setOutput] = useState("")
  const collect = useState(false)
  const result = useState(false)
  const features = {
    all: { name: "All", check: useState(false), click: 0, },
    line: { name: "Line break removeval", check: useState(false), click: 2, },
    addspace: { name: "Add space at the end of line", check: useState(false), click: 1, },
    space: { name: "Space removal", check: useState(false), click: 1, },
    full: { name: "Punctuations", check: useState(false), click: 1, },
  }

  const line = (banana) => features.addspace.check[0] ? banana.replace(/(\r\n|\n|\r)/gm, " ") : banana.replace(/(\r\n|\n|\r)/gm, "");
  const space = (banana) => banana.split(" ").join("");
  const full = (banana) => {
    const fMarks = [65311, 65281, 65292, 65307, 65306, 65288, 65289];
    const hMarks = [63, 33, 44, 59, 58, 40, 41];
    let text = ""
    for (let i = 0; i < banana.length; i++) { text += hMarks.includes(banana.charCodeAt(i)) ? String.fromCharCode(fMarks[hMarks.indexOf(banana.charCodeAt(i))]) : banana.charAt(i) }
    return text;
  }
  const Buttonset = (props) => {
    const go = () => ((props.set === "one" || props.set === "paste") ? navigator.clipboard.readText().then(text => { setInput(text); return text }) : new Promise(resolve => resolve(input)))
      .then(text => features.line.check[0] ? line(text) : text)
      .then(text => features.space.check[0] ? space(text) : text)
      .then(text => features.full.check[0] ? full(text) : text)
      .then(text => {
        setOutput(collect[0] & output !== "" ? output + "\r\n\r\n" + text : text);
        if (props.set === "one" || props.set === "copy") {
          document.querySelector("#minion").select()
          document.execCommand("copy")
        }
      })

    switch (props.set) {
      case "separated":
        return (<>
          <div className="col-sm-4 py-1"><button className="btn btn-secondary btn-block" type="button" onClick={() => navigator.clipboard.readText().then(text => setInput(text))}>Banana!</button></div>
          <div className="col-sm-4 py-1 px-sm-0"><button className="btn btn-secondary btn-block" type="button" onClick={go}>Roger!</button></div>
          <div className="col-sm-4 py-1"><button className="btn btn-secondary btn-block" type="button" onClick={() => setInput("")}>Eat banana!</button></div>
        </>)
      case "paste":
        return <div className="col"><button className="btn btn-secondary btn-block" type="button" onClick={go}>Gelato!</button></div>
      case "copy":
        return (<>
          <div className="col-sm-4 py-1"><button className="btn btn-secondary btn-block" type="button" onClick={() => navigator.clipboard.readText().then(text => setInput(text))}>Banana!</button></div>
          <div className="col-sm-4 py-1 px-sm-0"><button className="btn btn-secondary btn-block" type="button" onClick={go}>Para tu!</button></div>
          <div className="col-sm-4 py-1"><button className="btn btn-secondary btn-block" type="button" onClick={() => setInput("")}>Eat banana!</button></div>
        </>)
      case "one":
        return <div className="col"><button className="btn btn-secondary btn-block" type="button" onClick={go}>Go banana!</button></div>
      default:
        break;
    }
  }
  const mode = {
    id: ["separated", "paste", "copy", "one"],
    name: ["Papoy", "Gelato", "Para tu", "Banana"],
    description: ["One button, one function", "Paste and transform", "Transform and copy", "All in One"],
    state: useState("one")
  }
  return (
    <>
      <Card className="my-3">
        <Card.Header className="d-flex">
          <div className="mr-auto lead">Configuration</div>
          <Button variant="link" onClick={() => setConfig(!config)}><i className={config ? "fas fa-angle-up" : "fas fa-angle-down"} /></Button>
        </Card.Header>
        <Collapse in={config}>
          <Card.Body>
            <div className="form-group d-flex flex-column flex-sm-row">
              <div className="pr-3">Button mode:</div>
              {mode.id.map((item, index) => (<div className="custom-control custom-radio custom-control-inline" key={index} title={mode.description[index]}>
                <input type="radio" id={item} name="mode" className="custom-control-input" value={item} checked={mode.state[0] === item} onChange={e => { mode.state[1](e.target.value) }} />
                <label className="custom-control-label" htmlFor={item}>{mode.name[index]}</label>
              </div>))}
            </div>
            <div className="form-group d-flex flex-column flex-sm-row">
              <div className="pr-3">Features:</div>
              {Object.keys(features).map(item => (<div key={item} className="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" className="custom-control-input" id={item}
                  checked={item === "all" ? Object.keys(features).slice(2).every(element => features[element].check[0]) : features[item].check[0]}
                  disabled={item === "addspace" ? !features.line.check[0] : ""}
                  onChange={
                    e => {
                      switch (item) {
                        case "all":
                          return Object.keys(features).map(element => features[element].check[1](e.target.checked))
                        case "line":
                          return [features[item].check[1](e.target.checked), features.addspace.check[1](e.target.checked & features.addspace.check[0])]
                        default:
                          return features[item].check[1](e.target.checked)
                      }
                    }
                  } />
                <label className="custom-control-label" htmlFor={item}>{features[item].name}</label></div>))}
            </div>
            <div className="form-group">
              <div className="custom-control custom-switch">
                <input type="checkbox" className="custom-control-input" id="art" checked={collect[0]} onChange={e => { collect[1](e.target.checked); setOutput("") }} />
                <label className="custom-control-label" htmlFor="art">Collect bananas?</label>
              </div>
            </div>
          </Card.Body>
        </Collapse>
      </Card>
      <div className="form-group my-3">
        <label htmlFor="banana">Put banana here:</label>
        <textarea className="form-control" style={{ height: "15vh" }} value={input} onChange={e => setInput(e.target.value)} />
      </div>
      <div className="pb-3">
        <div className="py-1 row">
          <Buttonset set={mode.state[0]} />
          {/* <Button onClick={go}>Test</Button> */}
        </div>
      </div>
      <div className="form-group" style={{ position: "relative" }}>
        <label htmlFor="minion">Here is response from the minion:</label>
        <textarea className="form-control" id="minion" style={{ height: "15vh" }} value={output} readOnly />
        <button className="btn" style={{ position: "absolute", bottom: "15px", right: "15px" }} onClick={() => result[1](true)} ><i className="fas fa-search-plus" /></button>
        <Modal size="lg" show={result[0]} onHide={() => result[1](false)}>
          <Modal.Body><pre>{output}</pre></Modal.Body>
          <Modal.Footer><Button variant="primary" onClick={() => result[1](false)}>Hehe</Button></Modal.Footer>
        </Modal>
      </div>
      <div><button className="btn btn-success btn-block" type="button" onClick={e => { e.select(); document.execCommand("copy") }} hidden={mode.state[0] === "one" || mode.state[0] === "copy"}>Say Thank you to minion and take it away</button></div>
    </>
  )
}
export default App;
