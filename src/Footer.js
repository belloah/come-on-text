const Footer = (props) => <footer className="py-3 footer mt-auto"><div className="container text-center">{props.children}</div></footer>
export default Footer