const Title = (props) => <div className="pb-2 mt-4 mb-2 border-bottom"><h1>{props.children}<br className="d-block d-sm-none" /> <small>by Belloah</small></h1></div>
export default Title